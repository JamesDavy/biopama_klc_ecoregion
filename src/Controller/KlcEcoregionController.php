<?php
namespace Drupal\biopama_klc_ecoregion\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the biopama_klc_ecoregion module.
 */
class KlcEcoregionController extends ControllerBase {
  public function content() {
    $element = array(
	  '#theme' => 'klc_ecoregion',
	  '#test_var' => $this->t('Test Value'),
    );
    return $element;
  }
}