//TODO move into Dependancy with PA module.
var DOPAgetWdpaExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_wdpa_extent?format=json&wdpa_id=";
var mapPointHostURL = "https://tiles.biopama.org/BIOPAMA_point_2";
var mapPolyHostURL = "https://tiles.biopama.org/BIOPAMA_poly_2";
var mapPaLayer = "WDPA2019MayPoly";
var mapCountryLayer = "ACP_Countries";
var mapPaLabelsLayer = "WDPA2019MayPolyPoints";
var mapPaPointLayer = "WDPA2019MayPoints";
var mapSubRegionLayer = "ACP_SubGroups";
var mapSubRegionPointLayer = "ACP_SubGroups_points";
var regionSummary = {};
var countrySummary = {};//#of applications, total budget

jQuery(document).ready(function($) {
	mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
	var mymap = new mapboxgl.Map({
		container: 'map-container',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc', //Andrews default new RIS v2 style based on North Star
		attributionControl: true,
		renderWorldCopies: true,
		center: [26, -6.66],
        zoom: 2,
		minZoom: 1.4,
		maxZoom: 12
	});
	
	$(window).resize(function(){
		//resizeChart();
		var height = getWindowHeight();
		resizeMap(height);
	});
	function getWindowHeight(){
		var height = jQuery(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
		if (jQuery('#toolbar-item-administration-tray')[0]){
			var adminHeight = jQuery('#toolbar-item-administration-tray').height() + jQuery('#toolbar-bar').height();
			jQuery('#map-container').css('top', adminHeight);
			height = height - adminHeight;
		}
		return height;
	}
	function getMapPadding(){
		var mapPadding = {top: 100, bottom:200, left: 20, right: 0}
		var right = $('#block-bootstrap-barrio-biopama-content').width(); //get width of the content area
		mapPadding.right = (right / 2); //we know it's a 4 8 layout so the right side is pushed in 2/3rds.
		return mapPadding;
	}
	function resizeMap(height){
		jQuery('#map-container').css('height', height);
		mymap.resize();
	}
	
	var mapHeight = getWindowHeight();
	resizeMap(mapHeight);
	
	var mapPadding = getMapPadding();
	//25.136719,-38.959409,59.589844,38.959409
	mymap.fitBounds([
		[25.136719,-38.959409],
		[59.589844,38.959409]
	], {
	  padding: mapPadding
	});



	mymap.on('load', function () {
		let attribution = "KLC info! <a href='http://www.google.com'>Temp link to Google</a>";
		let tiles = ["https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/wmts?layer=biopama:klc_201909_proposal&tilematrixset=EPSG:900913&Service=WMTS&Request=GetTile&Version=1.0.0&Format=application/x-protobuf;type=mapbox-vector&TileMatrix=EPSG:900913:{z}&TileCol={x}&TileRow={y}"];
		mymap.addSource("klc-source",{
			"attribution": attribution,
			"type": "vector",
			"tiles": tiles 
		  }
		); 
		mymap.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addSource("BIOPAMA_Point", {
			"type": 'vector',
			"tiles": [mapPointHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addLayer({
			'id': 'klc-layer',
			'type': 'line',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			'paint': {
				"line-color": "hsl(115, 48%, 31%)",
				"line-width": 2,
			},
		},'country-label-sm');
		
		mymap.addLayer({
			"id": "wdpaBase",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
            "paint": {
                "fill-color": "hsla(87, 47%, 53%, 0.1)",
            }
		});
		
		mymap.addLayer({
			'id': 'klc-layer-hover',
			'type': 'line',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			"layout": {"visibility": "none"},
			"paint": {
				"line-color": "#ff6f1a",
				"line-width": 2,
			}
		});
		mymap.addLayer({
			'id': 'klc-layer-selected',
			'type': 'line',
			'source': "klc-source",
			'source-layer': 'klc_201909_proposal',
			"layout": {"visibility": "none"},
			"paint": {
				"line-color": "#90c14f",
				"line-width": 3,
			}
		});
		mymap.addLayer({
			"id": "wdpaAcpPolyLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaLabelsLayer,
			"minzoom": 5,
            "layout": {
                "text-field": ["to-string", ["get", "NAME"]],
                "text-size": 12,
                "text-font": [
                    "Arial Unicode MS Regular",
                    "Arial Unicode MS Regular"
                ]
            },
            "paint": {
                "text-halo-width": 2,
                "text-halo-blur": 2,
                "text-halo-color": "hsl(0, 0%, 100%)",
                "text-opacity": 1
            }
		}, 'country-label-sm');
		mymap.addLayer({
			"id": "wdpaAcpPointLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaPointLayer,
			"minzoom": 5,
            "layout": {
                "text-field": "{NAME}",
                "text-size": 12,
                "text-padding": 3,
				"text-offset": [0,-1]
            },
            "paint": {
                "text-color": "hsla(213, 49%, 13%, 0.95)",
                "text-halo-color": "hsla(0, 0%, 100%, .9)",
                "text-halo-width": 2,
                "text-halo-blur": 2
            }
		}, 'country-label-sm');
		
	});
	mymap.on('idle', () => {
		var displayProperties = [
			'properties',
		];
		var features = mymap.queryRenderedFeatures({layers: ['klc-layer']});
		
		var displayFeatures = features.map(function(feat) {
			var displayFeat = {};
			displayFeat = feat["properties"];
			return displayFeat;
		});
		var unique = _.uniq(_.map(displayFeatures, "klc_name"));
		unique.sort();
		if ($("#klc-accordion.ui-accordion").length > 0){
			$("#klc-accordion").accordion( "destroy" );
		}
		$("#klc-accordion").empty();
		unique.forEach(function(klc) {
			var newline = $( "<h3><i class='fas fa-chevron-circle-down'></i>"+klc+"</h3><div>Temp</div>" );
			$( "#klc-accordion" ).append( newline );
		});
		updateKLClist(unique);
	});
	
	function updateKLClist(KLCs){
		$( "#klc-accordion" ).accordion({
		  collapsible: true,
		  active: false,
		  heightStyle: "content",
		  beforeActivate: function( event, ui ) {
			  if (ui.newPanel.length > 0){
				var klcFeatures = mymap.querySourceFeatures('klc-source', {
					sourceLayer: 'klc_201909_proposal',
					filter: ['in', 'klc_name', ui.newHeader[0].innerText]
				});
				KLCinfo = "<div>"+
					"<div class='row'>"+
						"<div class='col-sm-6'>"+
						"<table>"+
							"<tr><td>Area</td><td>"+parseFloat(klcFeatures[0].properties.area_km2).toFixed(3)+"km&sup2;</td></tr>"+
							"<tr><td>KLC ID</td><td>"+klcFeatures[0].properties.klc_id+"</td></tr>"+
							"<tr><td>Target PAs</td><td>"+klcFeatures[0].properties.target_pas+"</td></tr>"+
							"<tr><td>Comments</td><td>"+klcFeatures[0].properties.comments+"</td></tr>"+
							"</table>"+
						"</div>"+
						"<div class='col-sm-6 klc-chart'>"+
							"<img class='temp-klc-chart' src='/modules/custom/biopama_klc_ecoregion/img/chart1.png'>"+
						"</div>"+
					"</div>"+
					"<div class='row'>"+
						"<div class='col-sm-6 klc-chart'>"+
							"<img class='temp-klc-chart' src='/modules/custom/biopama_klc_ecoregion/img/chart2.png'>"+
						"</div>"+
						"<div class='col-sm-6 klc-chart'>"+
							"<img class='temp-klc-chart' src='/modules/custom/biopama_klc_ecoregion/img/chart3.png'>"+
						"</div>"+
					"</div>"+
				"</div>";
				//mymap.setFilter('klc-layer-hover', ['==', 'klc_name', ui.newHeader[0].innerText]);
				//mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'visible');
				ui.newPanel[0].innerHTML = KLCinfo;
				//console.log(klcFeatures)
			  } else {
				//mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'none');	
			  }
		  }
		});
/* 		$( ".container-fluid #accordion h3" ).mouseenter(function() {
			console.log("test")
			var klcTitle = $( this ).text();
			mymap.setFilter('klc-layer-hover', ['==', 'klc_name', klcTitle]);
			mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'visible');	
		}).mouseleave(function() {
			mymap.setLayoutProperty('klc-layer-hover', 'visibility', 'none');	
		}); */
		$( "#klc-accordion" ).show("fast");
	}
	
	
	
});
